import { initStore } from 'react-waterfall';
import { compose, prop, propEq, reject, map } from 'ramda';
import { getData, userLogin } from 'utils/api';

const removeRowById = ({ id, type }) => compose(
  reject(propEq('id', id)),
  prop(type)
)

const toggleConfirmedById = ({ id, type }) => compose(
  map((element) => prop('id', element) === id
    ? { ...element, confirmed: !Boolean(element.confirmed) }
    : element
  ),
  prop(type)
);

const updateTypesById = ({ updatedEntity, type }) => compose(
  map((element) => prop('id', element) === updatedEntity.id
    ? updatedEntity
    : element
  ),
  prop(type)
);

const store = {
  initialState: {
    data: {},
    user: undefined,
    errorMessage: false
  },
  actions: {
    getData: async () => {
      const data = await getData();

      return { data };
    },
    removeRow: ({ data }, type, id) => {
      const typeNewData = removeRowById({ id, type })(data);

      return { data: { ...data, [type]: typeNewData }};
    },
    toggleConfirmed: ({ data }, type, id) => {
      const typeNewData = toggleConfirmedById({ id, type })(data);

      return { data: { ...data, [type]: typeNewData }};
    },
    addNewEntity: ({ data }, type, newEntity) => (
      { data: { ...data, [type]: [...data[type], newEntity] }}
    ),
    updateEntity: ({ data }, type, updatedEntity) => {
      const typeNewData = updateTypesById({ updatedEntity, type })(data);

      return { data: { ...data, [type]: typeNewData }};
    },
    userLogin: async ( _, username, password) => {
      const user = await userLogin(username, password);

      return { user };
    },
    setErrorMessage: (_, errorMessage) => ({ errorMessage }),
    clearErrorMessage: () => ({ errorMessage: false })
  }
}

export const {
  Provider,
  Consumer,
  actions,
  getStatem,
  connect,
  subscribe
} = initStore(store);
