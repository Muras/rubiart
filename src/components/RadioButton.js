import React from 'react';
import PropTypes from 'prop-types';
import RadioButtonChecked from 'material-ui-icons/RadioButtonChecked';
import RadioButtonUnchecked from 'material-ui-icons/RadioButtonUnchecked';


function RadioButtonIcon({ checked }) {
  return Boolean(checked) ?
    <RadioButtonChecked />
    : <RadioButtonUnchecked />
}

RadioButtonIcon.propTypes = {
  checked: PropTypes.number
}

export default RadioButtonIcon;
