import React from 'react';
import PropTypes from 'prop-types';
import Tooltip from 'material-ui/Tooltip';
import InfoOutline from 'material-ui-icons/InfoOutline';
import { withStyles } from 'material-ui/styles';

const styles = theme => ({
  popper: {
    maxWidth: 300,
  },
  tooltip: {
    fontSize: 16,
    fontWeight: 300,
    background: 'grey',    
  }
});

const ExtraInformation = props => {
  const { classes, informations } = props;

  return (
    <Tooltip classes={{
      popper: classes.popper,
      tooltip: classes.tooltip,
    }} placement="right" title={informations}>
      <InfoOutline />
    </Tooltip>
  );
};

ExtraInformation.propTypes = {
  informations: PropTypes.string,
};

export default withStyles(styles)(ExtraInformation);