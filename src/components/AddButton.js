import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import AddIcon from 'material-ui-icons/AddCircleOutline';

const styles = theme => ({
  button: {
    margin: theme.gutters[16],
    width: 200,
    fontSize: '0.9rem',
  },
  icon: {
    width: 16,
    heigh: 16,
    marginLeft: theme.gutters[8],
    color: theme.palette.common.white
  }
});


function AddButton({ classes, onClick, text }) {
  return (
    <Button
      onClick={onClick}
      color="secondary"
      variant="raised"
      className={classes.button}
    >{text}
      <AddIcon className={classes.icon} />
    </Button>
  );
}

AddButton.propTypes = {
  onClick: PropTypes.func,
  text: PropTypes.string,
};

export default withStyles(styles)(AddButton);
