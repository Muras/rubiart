import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import { CircularProgress } from 'material-ui/Progress';

const styles = theme => ({
  wrapper: {
    height: 300,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  }
});

function ModalProgress({ classes }) {
  return (
    <div className={classes.wrapper}>
      <CircularProgress className={classes.progress} size={50} color="secondary" />
    </div>
  );
}

ModalProgress.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ModalProgress);
