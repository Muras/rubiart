import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import Snackbar from 'containers/Snackbar';
import CssBaseline  from 'material-ui/CssBaseline';
import { MuiThemeProvider  } from 'material-ui/styles';
import LoginPage from 'pages/LoginPage';
import VenuesPage from 'pages/VenuesPage/VenuesPage';
import TrainersPage from 'pages/TrainersPage/TrainersPage';
import IndexLayout from 'layout/IndexLayout';
import stateSetter from 'utils/stateSetter';
import theme from 'styles/theme';
import { Provider, actions, subscribe } from 'store';

const EmptyComponent = () => <div>Dummy Component</div>;

subscribe((action, state) => console.log(action, state))

class App extends React.Component {
  constructor() {
    super();

    this.setter = new stateSetter(this);
    this.state = {
      isAuthenticated: false,
      data: {},
    }
  }

  componentWillUnmount() {
    this.setter.cancel();
  }

  authenticateUser = () => {
    this.setter.setState({ isAuthenticated: true })
  }

  startApplication = async () => {
    this.authenticateUser();
    actions.getData();
  }

  applicationRoutes = () => {
    return (
      <Switch>
        <Route path="/grupy" render={(props) => <EmptyComponent {...props} />} />
        <Route path="/obiekty" component={VenuesPage} />
        <Route path="/zgloszenia" render={(props) => <EmptyComponent {...props} />} />
        <Route path="/trenerzy" component={TrainersPage} />
        <Redirect path="*" to="/grupy" />      
      </Switch>
    );
  };

  render() {
    return (
      <Provider>
        <div className="App">
          <Snackbar />
          <MuiThemeProvider theme={theme}>
            <Router>
              <CssBaseline>
                {this.state.isAuthenticated ?
                  <IndexLayout>{this.applicationRoutes()}</IndexLayout>
                  : <LoginPage onLoginSuccess={this.startApplication} />
                }
              </CssBaseline>
            </Router>
          </MuiThemeProvider>
        </div>
      </Provider>
    );
  }
}

export default App;
