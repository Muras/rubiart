import React from 'react';
import PropTypes from 'prop-types';
import { CircularProgress } from 'material-ui/Progress';
import { actions } from 'store';
import ConfirmIcon from '../Icons/Confirm';

class RowConfirm extends React.Component {
  constructor() {
    super();

    this.state = {
      sendingRequest: false,
    };
  }

  handleConfirm = async (event) => {
    event.preventDefault();
    this.setState({ sendingRequest: true });

    const { rowData: { id, confirmed }, onConfirm, type } = this.props

    try {
      await onConfirm({ type, id, confirmed });
      actions.toggleConfirmed(type, id);
    } catch(exception) {
      actions.setErrorMessage('Problem z zapisem, spróbuj ponownie.');
    }

    this.setState({ sendingRequest: false });
  };

  render() {
    const { rowData: { confirmed } } = this.props;

    return this.state.sendingRequest ?
      <CircularProgress size={15} />
      : <ConfirmIcon confirmed={confirmed} onClick={this.handleConfirm} />
  }
}

RowConfirm.propTypes = {
  onConfirm: PropTypes.func.isRequired,
  rowData: PropTypes.shape({
    id: PropTypes.number.isRequired,
    confirmed: PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.number
    ]).isRequired
  })
};

export default RowConfirm;
