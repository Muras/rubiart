import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { CircularProgress } from 'material-ui/Progress';
import { actions } from 'store';
import DeleteIcon from '../Icons/Delete';

class RowDelete extends React.Component {
  constructor() {
    super();

    this.state = {
      sendingRequest: false,
    };
  }

  handleDelete = async (event) => {
    event.preventDefault();
    this.setState({ sendingRequest: true });

    const { rowData: { id }, onDelete, type } = this.props
    
    try {
      await onDelete({ type, id });
      actions.removeRow(type, id)
    } catch(exception) {
      actions.setErrorMessage('Problem z usunięciem, spróbuj ponownie.');
    }

    this.setState({ sendingRequest: false });
  };

  render() {
    const { sendingRequest } = this.state;

    return (
      <Fragment>
        {sendingRequest ?
          <CircularProgress size={15} />
          : <DeleteIcon onClick={this.handleDelete} />}
      </Fragment>
    );
  }
}

RowDelete.propTypes = {
  onDelete: PropTypes.func.isRequired,
};

export default RowDelete;
