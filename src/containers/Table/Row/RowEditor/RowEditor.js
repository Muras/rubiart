import React from 'react';
import PropTypes from 'prop-types';
import AddButton from 'components/AddButton';
import { withStyles } from 'material-ui/styles';
import RowEditorStyles from 'styles/jss/containers/Table/RowEditorStyles';
import Dialog from 'material-ui/Dialog';
import { actions } from 'store';
import CoachesEditor from './CoachesEditor';
import VenuesEditor from './VenuesEditor';
import Edit from '../../Icons/Edit';
import dataTable from "../../dataTable";


class RowEditor extends React.Component {
  constructor() {
    super();

    this.state = {
      open: false,
      submitData: false
    };
  }

  async createNewEntity(newData, type) {
    const responseData = await dataTable.onCreate({
      newData: { ...newData, confirmed: false },
      type
    });

    actions.addNewEntity(type, responseData);
  }

  async editEntitiy(newData, type) {
    const responseData = await this.props.onEdit({ type, newData });

    actions.updateEntity(type, responseData);
    this.setState({ open: false });    
  }

  handleOnSubmit = async (newData) => {
    const { newEntity, type } = this.props;
    try {
      if (newEntity) {
        await this.createNewEntity(newData, type);
        this.setState({ open: false });        
      } else {
        await this.editEntitiy(newData, type);
        this.setState({ open: false });
      }
    } catch (exception) {
      actions.setErrorMessage('Nieprawidłowe dane lub nie wszysktie pola zostały wypełnione.');
    }
  }

  setEditorOpen = (value) => () => {
    this.setState({ open: value });
  }

  renderEdtior = () => {
    const newAttributes = {
      onSubmit: this.handleOnSubmit,
      closeDialog: this.setEditorOpen(false)
    };
    const { type } = this.props;

    if (type === 'coaches') {
      return <CoachesEditor {...this.props} {...newAttributes} />;
    } else if (type === 'venues') {
      return <VenuesEditor {...this.props} {...newAttributes} />;
    }
  }

  render() {
    const { classes, newEntity, type } = this.props;
    const { open } = this.state;
    
    return (
      <div>
        {newEntity ?
          <AddButton onClick={this.setEditorOpen(true)} text={dataTable[type].newTitle} />
          : <Edit onClick={this.setEditorOpen(true)} />
        }
        {open && (
          <Dialog
            classes={{ paperWidthSm: classes.dialog }}
            open={open}
            onClose={this.setEditorOpen(false)}
          >
            {this.renderEdtior()}
          </Dialog>
        )}
      </div>
    );
  }
}

RowEditor.propTypes = {
  newEntity: PropTypes.bool,
  open: PropTypes.bool,
  title: PropTypes.string,
  rowData: PropTypes.object,
  onEdit: PropTypes.func,
  headerLabels: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string,
    sorter: PropTypes.string,
  })),
};

export default withStyles(RowEditorStyles)(RowEditor);
