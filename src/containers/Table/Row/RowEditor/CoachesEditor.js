import React from "react";
import PropTypes from "prop-types";
import Button from "material-ui/Button";
import TextField from "material-ui/TextField";
import Grid from "material-ui/Grid";
import { CircularProgress } from "material-ui/Progress";
import { withStyles } from "material-ui/styles";
import RowEditorStyles from "styles/jss/containers/Table/RowEditorStyles";
import {
  DialogActions,
  DialogContent,
  DialogTitle
} from "material-ui/Dialog";
import { getLabel } from "utils/tableHelpers";
import dataTable from "../../dataTable";


class CoachesEditor extends React.Component {
  constructor() {
    super();

    this.state = {
      open: false,
      sendingRequest: false
    };
  }

  componentWillMount() {
    if (this.props.newEntity) {
      return;
    }

    const {
      id = "",
      street = "",
      name = "",
      surname = "",
      city = "",
      email = "",
      tel = "",
      extra_information = "",
      postal_code = ""
    } = this.props.rowData;

    this.setState({
      id,
      street,
      name,
      surname,
      city,
      email,
      tel,
      extra_information,
      postal_code
    });
  }

  handleSubmit = async event => {
    event.preventDefault();
    this.setState({ sendingRequest: true });

    const {
      id,
      street,
      name,
      surname,
      city,
      email,
      tel,
      extra_information,
      postal_code
    } = this.state;

    const newData = {
      id,
      street,
      name,
      surname,
      city,
      email,
      tel,
      extra_information,
      postal_code
    };

    await this.props.onSubmit(newData);    

    this.setState({ sendingRequest: false });
  };

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  render() {
    const { classes, newEntity } = this.props;
    const { sendingRequest } = this.state;
    const { labels, title, newTitle } = dataTable[this.props.type];

    return (
      <form onSubmit={this.handleSubmit}>
        <DialogTitle id="form-nowy-trener">
          {newEntity ? newTitle : title}
        </DialogTitle>
        <DialogContent>
          <Grid container classes={{ typeContainer: classes.container }}>
            <Grid item classes={{ typeItem: classes.column }}>
              <TextField
                margin="dense"
                onChange={this.handleChange("name")}
                InputProps={{ classes: { input: classes.textField } }}
                label={getLabel("name")(labels)}
                value={this.state.name}
              />
              <TextField
                margin="dense"
                onChange={this.handleChange("surname")}
                InputProps={{ classes: { input: classes.textField } }}
                label={getLabel("surname")(labels)}
                value={this.state.surname}
              />
              <TextField
                margin="dense"
                onChange={this.handleChange("street")}
                InputProps={{ classes: { input: classes.textField } }}
                label={getLabel("street")(labels)}
                value={this.state.street}
              />
              <TextField
                margin="dense"
                onChange={this.handleChange("postal_code")}
                InputProps={{ classes: { input: classes.textField } }}
                label={getLabel("postal_code")(labels)}
                value={this.state.postal_code}
              />
              <TextField
                margin="dense"
                onChange={this.handleChange("city")}
                InputProps={{ classes: { input: classes.textField } }}
                label={getLabel("city")(labels)}
                value={this.state.city}
              />
              <TextField
                margin="dense"
                onChange={this.handleChange("tel")}
                InputProps={{ classes: { input: classes.textField } }}
                label={getLabel("tel")(labels)}
                value={this.state.tel}
              />
              <TextField
                margin="dense"
                onChange={this.handleChange("email")}
                InputProps={{ classes: { input: classes.textField } }}
                label={getLabel("email")(labels)}
                value={this.state.email}
              />
            </Grid>
            <Grid item classes={{ typeItem: classes.column }}>
              <TextField
                multiline
                rowsMax="10"
                margin="dense"
                onChange={this.handleChange("extra_information")}
                InputProps={{ classes: { input: classes.multiline } }}
                label={getLabel("extra_information")(labels)}
                value={this.state.extra_information}
              />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <Button color="primary" type="submit" disabled={sendingRequest}>
            {sendingRequest ? (
              <CircularProgress size={24} color="inherit" />
            ) : (
              "Zatwierdź"
            )}
          </Button>
          <Button onClick={this.props.closeDialog} color="primary">
            Anuluj
          </Button>
        </DialogActions>
      </form>
    );
  }
}

CoachesEditor.propTypes = {
  newEntity: PropTypes.bool,
  closeDialog: PropTypes.func,
  open: PropTypes.bool,
  title: PropTypes.string,
  rowData: PropTypes.object,
  onEdit: PropTypes.func,
  headerLabels: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string,
      sorter: PropTypes.string
    })
  )
};

export default withStyles(RowEditorStyles)(CoachesEditor);
