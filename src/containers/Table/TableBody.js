import React from 'react';
import PropTypes from 'prop-types';
import { TableBody as TableBodyMUI, TableRow } from 'material-ui/Table';
import { getProperRowsCellValues } from 'utils/tableHelpers';
import TableCell from './TableCell';
import RowEditor from './Row/RowEditor/RowEditor';
import RowDelete from './Row/RowDelete';
import RowConfirm from './Row/RowConfirm';
import dataTable from './dataTable';


function TableBody(props) {
  const { rows, rowKeys, type } = props;
  const { onDelete, onEdit, onConfirm } = dataTable;

  return (
    <TableBodyMUI>
      {rows.map(row => (
        <TableRow key={row.id}>
          {rowKeys.map(key => 
            <TableCell key={key}>{getProperRowsCellValues(row, key, type)}</TableCell>
          )}
          <TableCell><RowDelete onDelete={onDelete} type={type} rowData={row} /></TableCell>
          <TableCell><RowEditor onEdit={onEdit} type={type} rowData={row} /></TableCell>
          <TableCell><RowConfirm onConfirm={onConfirm} type={type} rowData={row} /></TableCell>
        </TableRow>
      ))}
    </TableBodyMUI>
  );
}


TableBody.propTypes = {
  type: PropTypes.string,
  rows: PropTypes.array,
  rowKeys: PropTypes.array,
};

export default TableBody;
