import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import CheckCircle from 'material-ui-icons/CheckCircle';
import IconButton from './IconButton';

const styles = theme => ({
  accepted: {
    color: theme.palette.success
  },
});

function ConfirmIcon({ confirmed, onClick, classes }) {
  const acceptedClassName = confirmed ? classes.accepted : '';

  return (
    <IconButton onClick={onClick}>
      <CheckCircle className={acceptedClassName} />
    </IconButton>
  );
}

ConfirmIcon.propTypes = {
  onClick: PropTypes.func,
  confirmed: PropTypes.oneOfType([
    PropTypes.bool,
    PropTypes.number
  ]),
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ConfirmIcon);
