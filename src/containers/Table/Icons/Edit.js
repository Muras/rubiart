import React from 'react';
import PropTypes from 'prop-types';
import Edit from 'material-ui-icons/Edit';
import IconButton from './IconButton';

function DeleteIcon({ onClick }) {
  return (
    <IconButton onClick={onClick}>
      <Edit />
    </IconButton>
  );
}

DeleteIcon.propTypes = {
  onClick: PropTypes.func.isRequired,
};

export default DeleteIcon;
