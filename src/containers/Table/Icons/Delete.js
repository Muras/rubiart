import React from 'react';
import PropTypes from 'prop-types';
import DeleteForever from 'material-ui-icons/DeleteForever';
import IconButton from './IconButton';

function DeleteIcon({ onClick }) {
  return (
    <IconButton onClick={onClick}>
      <DeleteForever />
    </IconButton>
  );
}

DeleteIcon.propTypes = {
  onClick: PropTypes.func,
};

export default DeleteIcon;
