import {
  updateEntity,
  createNewEntity,
  deleteEntity
} from 'utils/api';

export default {
  onConfirm: async ({ type, id, confirmed }) => await updateEntity(type, { id, confirmed: !confirmed }),
  onEdit: async ({ type, newData }) => await updateEntity(type, newData),  
  onDelete: async ({ type, id }) => await deleteEntity(type, id),  
  onCreate: async ({ type, newData }) => await createNewEntity(type, newData),
  coaches: {
    title: 'Edytuj trenera',
    newTitle: 'Stwórz trenera',
    labels: [
      {
        label: "Imie",
        key: "name"
      },
      {
        label: "Nazwisko",
        key: "surname"
      },
      {
        label: "Kod pocztowy",
        key: "postal_code"
      },
      {
        label: "Miasto",
        key: "city"
      },
      {
        label: "Ulica",
        key: "street"
      },
      {
        label: "telefon",
        key: "tel"
      },
      {
        label: "e-mail",
        key: "email"
      },
      {
        label: "Dodatkowe informacje",
        key: "extra_information"
      }
    ],
  },
  venues: {
    title: 'Edytuj obiekt',
    newTitle: 'Stwórz obiekt',
    labels: [
      {
        label: "Nazwa obiektu",
        key: "name"
      },
      {
        label: "Kod pocztowy",
        key: "postal_code"
      },
      {
        label: "Miasto",
        key: "city"
      },
      {
        label: "Ulica",
        key: "street"
      },
      {
        label: "pn",
        key: "monday"
      },
      {
        label: "wt",
        key: "tuesday"
      },
      {
        label: "śr",
        key: "wednesday"
      },
      {
        label: "cw",
        key: "thursday"
      },
      {
        label: "pt",
        key: "friday"
      },
      {
        label: "Osoba kontaktowa",
        key: "contact_person"
      },
      {
        label: "telefon",
        key: "tel"
      },
      {
        label: "e-mail",
        key: "email"
      },
      {
        label: "Dodatkowe informacje",
        key: "extra_information"
      },
    ]
  }
};
