import React from 'react';
import PropTypes from 'prop-types';
import Button from 'material-ui/Button';
import SnackbarMUI from 'material-ui/Snackbar';
import { Consumer } from 'store';


function Snackbar() {
  return (
    <Consumer select={['errorMessage']}>
      {( { state, actions }) => (
        <SnackbarMUI
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}
          open={Boolean(state.errorMessage)}
          autoHideDuration={6000}
          onClose={actions.clearErrorMessage}
          SnackbarContentProps={{
            'aria-describedby': 'message-id',
          }}
          message={state.errorMessage}
          action={[
            <Button
              key="close"
              color="secondary"
              size="small"
              onClick={actions.clearErrorMessage}
            >
              Zamknij
            </Button>
          ]}
        />
      )}
    </Consumer>
  );
}

Snackbar.propTypes = {
  message: PropTypes.string,
};

export default Snackbar;
