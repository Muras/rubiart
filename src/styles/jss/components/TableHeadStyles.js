export default theme => ({
  root: {
    padding: `0 ${theme.gutters[4]}`,
    textAlign: 'center'
  },
  headerCell: {
    '&:hover': {
      cursor: 'pointer',
      background: theme.palette.grey[100]
    },
    '&:active': {
      background: theme.palette.grey[300]
    },
    transition: `all ${theme.transitionTime} ease-in`,
    padding: `0 ${theme.gutters[4]}`,
    textAlign: 'center'
  },
  tableHeadRow: {
    background: theme.palette.primary.light,
    fontSize: 18
  }
});
