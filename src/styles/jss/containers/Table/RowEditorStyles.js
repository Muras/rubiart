export default (theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'row'
  },
  column: {
    margin: 'auto',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  multiline: {
    height: 195,
    width: 300    
  },
  root: {
    width: '100%',
    margin: 30,
    overflowX: 'auto',
  },
  textField: {
    width: 350
  },
  dialog: {
    maxWidth: 800
  }
})
