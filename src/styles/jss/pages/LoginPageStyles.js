export default (theme) => ({
  wrapper: {
    background: theme.palette.grey[300],
    width: '100%',
    height: '100vh',
    display: 'flex',
    justifyContent: 'center',
  },
  paper: {
    marginTop: theme.gutters[32],    
    width: '100%',
    textAlign: 'center',
    padding: theme.gutters[40], 
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
  },
});
