export default (theme) => ({
  wrapper: {
    margin: 'auto',
    padding: 0,
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column'
  },
  root: {
    width: '100%',
    margin: 0,
    overflowX: 'auto',
  }
})

