export default theme => ({
  title: {
    padding: `${theme.gutters[32]} ${theme.gutters[40]}`,
    color: theme.palette.common.white,  
  },
});
