export default theme => {
  console.log(theme)
  return ({
  root: {
    ...theme.typography.button,
    padding: `${theme.gutters[12]} ${theme.gutters[20]}`,
    color: theme.palette.common.white,
    textDecoration: 'none',
    transition: `all ${theme.transitionTime} ease-in`
  },
  active: {
    background: theme.palette.primary.light
  }
})}

