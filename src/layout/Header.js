import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Typography from 'material-ui/Typography';
import AppBar from 'material-ui/AppBar';
import HeaderStyles from 'styles/jss/layout/HeaderStyles';


const Header = props => {
  const { classes, children } = props;

  return (
    <AppBar position="static">
      <Typography variant="display3" className={classes.title}>
        Panel administracyjny Rubiart
      </Typography>
      {children}
    </AppBar>
  );
};

Header.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

export default withStyles(HeaderStyles)(Header);