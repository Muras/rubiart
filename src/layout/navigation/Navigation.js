import React from 'react';
import PropTypes from 'prop-types';
import Toolbar from 'material-ui/Toolbar';
import NavigationLink from './NavigationLink';

const Navigation = props => {
  return (
    <Toolbar>
      <NavigationLink to='/obiekty'>Obiekty</NavigationLink>
      <NavigationLink to='/grupy'>Grupy</NavigationLink>
      <NavigationLink to='/zgloszenia'>Zgłoszenia</NavigationLink>
      <NavigationLink to='/trenerzy'>Trenerzy</NavigationLink>
    </Toolbar>
  );
};

Navigation.propTypes = {
  classes: PropTypes.object,
};

export default Navigation;