import React from 'react';
import PropTypes from 'prop-types';
import Navigation from './navigation/Navigation';
import Header from './Header';

const IndexLayout = (props) => {
  return (
    <div>
      <Header>
        <Navigation />
      </Header>
      {props.children}
    </div>
  );
};

IndexLayout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

export default IndexLayout;