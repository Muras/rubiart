import React from 'react';
import Tooltip from 'components/Tooltip';
import RadioButton from 'components/RadioButton';
import { find, propEq, compose, prop } from 'ramda';

const weekDays = [
  "monday",
  "tuesday",
  "wednesday",
  "thursday",
  "friday"
];

const specificTypesValues = {
  coaches: [
    (row, key) => key === 'name' && `${row.name} ${row.surname}`
  ],
  venues: [
    (row, key) => weekDays.some(day => day === key) && <RadioButton checked={row[key]} />
  ]
}

export function getProperRowsCellValues(row, key, type) {
  if (key === 'extra_information') {
    return <Tooltip informations={row[key]} />;
  }

  let result = row[key];
  specificTypesValues[type].forEach(valueCheck => {
    const value = valueCheck(row, key);

    if (value) {
      result = value;
    }
  });

  return result;
}

export const getLabel = (key) => compose(
  prop('label'),
  find(propEq('key', key))
);