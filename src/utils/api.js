
import { omit } from 'ramda';

const url = 'https://rubipanel-api.herokuapp.com/api';
let authrozationHash = '';

export async function userLogin(username, password) {
  authrozationHash = btoa(`${username}:${password}`);
  Object.freeze(authrozationHash);

  const response = await fetch(`${url}/user`, {
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      Authorization: `Basic ${authrozationHash}`
    },
    method: 'GET',
    mode: 'cors',
  });

  if (!response.ok) {
    throw new Error(response.statusText);
  }

  return await response.json();
}

export async function getData() {
  const response = await fetch(`${url}/data`, {
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      Authorization: `Basic ${authrozationHash}`,
      'content-type': 'application/json'
    },
    method: 'GET',
    mode: 'cors',
  });

  if (!response.ok) {
    throw new Error(response.statusText);
  }

  return await response.json();
}

export async function createNewEntity(type, data) {
  const serializedData = JSON.stringify(data);

  const response = await fetch(`${url}/${type}`, {
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      Authorization: `Basic ${authrozationHash}`,
      'content-type': 'application/json'
    },
    method: 'POST',
    body: serializedData
  });

  if (!response.ok) {
    throw new Error(response.statusText);
  }

  return await response.json();
  
}

export async function updateEntity(type, newData) {
  const serializedData = JSON.stringify(omit(['id'], newData));

  const response = await fetch(`${url}/${type}/${newData.id}`, {
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      Authorization: `Basic ${authrozationHash}`,
      'content-type': 'application/json'
    },
    method: 'POST',
    mode: 'cors',
    body: serializedData
  });

  if (!response.ok) {
    throw new Error(response.statusText);
  }

  return await response.json();
}

export async function deleteEntity(type, id) {
  const response = await fetch(`${url}/${type}/${id}`, {
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      Authorization: `Basic ${authrozationHash}`,
    },
    method: 'DELETE',
    mode: 'cors',
  });

  if (!response.ok) {
    throw new Error(response.statusText);
  }

  return await response.json();
}
