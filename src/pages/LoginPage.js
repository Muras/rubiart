import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from 'material-ui/Button';
import { CircularProgress } from 'material-ui/Progress';
import Typography from 'material-ui/Typography';
import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import Grid from 'material-ui/Grid';
import TextField from 'material-ui/TextField';
import StateSetter from 'utils/stateSetter';
import LoginPageStyles from 'styles/jss/pages/LoginPageStyles';
import { userLogin } from 'utils/api';
import { actions } from 'store';

class LoginPage extends Component {
  constructor() {
    super();

    this.setter = new StateSetter(this);
    this.state = {
      username: '',
      password: '',
      isSaving: false,
    }
  }

componentWillUnmount() {
  this.setter.cancell();
}

  onInputChange = (event) => {
    const { value, id } = event.target;

    this.setState({ [id]: value });
  }

  toggleIsSaving(value) {
    this.setter.setState({ isSaving: value });
  }

  onLoginSubmit = async (event) => {
    event.preventDefault();
    this.toggleIsSaving(true);
    const { username, password } = this.state;

    try {
      await userLogin(username, password);
      this.props.onLoginSuccess();
    } catch (exception) {
      actions.setErrorMessage('Problemy z logowaniem!');
    }

    this.toggleIsSaving(false);    
  }

  render() {
    const { classes } = this.props;
    const { username, password, isSaving } = this.state;

    return (
      <div className={classes.wrapper}>
        <Grid item xs={12} sm={6}>
          <Paper classes={{ root: classes.paper }} elevation={8}>
            <form className={classes.form} onSubmit={this.onLoginSubmit}>
              <Typography variant="display2">
                Zaloguj się
              </Typography>
              <TextField
                id="username"
                label="Użytkownik"
                className={classes.textField}
                value={username}
                onChange={this.onInputChange}
                margin="normal"
              />
              <TextField
                id="password"
                label="Hasło"
                className={classes.textField}
                value={password}
                onChange={this.onInputChange}
                type="password"
                margin="normal"
              />
              <Button variant="raised" color="primary" type="submit" disabled={isSaving}>
                {isSaving ?
                  <CircularProgress size={24} color="inherit" />
                  : 'Zaloguj'
                }
              </Button>
            </form>
          </Paper>
        </Grid>
      </div>
    );
  }
}

LoginPage.propTypes = {
  onLoginSuccess: PropTypes.func.isRequired,
};

export default withStyles(LoginPageStyles)(LoginPage);