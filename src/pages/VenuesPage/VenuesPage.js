import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import Grid from 'material-ui/Grid';
import CenteredProgress from 'components/CenteredProgress';
import VenuesPageStyles from 'styles/jss/pages/VenuesPageStyles'
import Table from 'containers/Table/Table';
import RowEditor from 'containers/Table/Row/RowEditor/RowEditor';
import { Consumer } from 'store';
import { labelsMap, rowKeys, type } from './data';


class VenuesPage extends Component {
  render() {
    const { classes } = this.props;
    
    return (
      <Consumer select={['data']}>
      {({ state }) => (
        <Grid item md={10} className={classes.wrapper}>
          <RowEditor type={type} newEntity />
          <Paper className={classes.root}>
            {state.data.venues ?
              <Table
                type={type}
                rows={state.data.venues}
                headerLabels={labelsMap}
                rowKeys={rowKeys}
              />
              : <CenteredProgress size={40} />
            }
          </Paper>
        </Grid>
      )}
      </Consumer>
    );
  }
}

VenuesPage.propTypes = {
  venues: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    surname: PropTypes.string,
    postal_code: PropTypes.string,
    city: PropTypes.string,
    street: PropTypes.string,
    tel: PropTypes.string,
    email: PropTypes.string,
    confirmed: PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.number
    ]),
    extra_information: PropTypes.string,
  },))
};

export default withStyles(VenuesPageStyles)(VenuesPage);
