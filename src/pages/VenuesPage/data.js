export const labelsMap = [
  {
    label: "Nazwa obiektu",
    sorter: "name"
  },
  {
    label: "Kod pocztowy",
    sorter: "postal_code"
  },
  {
    label: "Miasto",
    sorter: "city"
  },
  {
    label: "Ulica",
    sorter: "street"
  },
  {
    label: "pn",
    sorter: "monday"
  },
  {
    label: "wt",
    sorter: "tuesday"
  },
  {
    label: "śr",
    sorter: "wednesday"
  },
  {
    label: "cw",
    sorter: "thursday"
  },
  {
    label: "pt",
    sorter: "friday"
  },
  {
    label: "Osoba kontaktowa",
    sorter: "contact_person"
  },
  {
    label: "telefon",
    sorter: "tel"
  },
  {
    label: "e-mail",
    sorter: "email"
  },
  {
    label: "Dodatkowe informacje",
    sorter: ""
  },
];

export const rowKeys = [
  "name",
  "postal_code",
  "city",
  "street",
  "monday",
  "tuesday",
  "wednesday",
  "thursday",
  "friday",
  "contact_person",
  "tel",
  "email",
  "extra_information"
];

export const type = 'venues';
