export const labelsMap = [
  {
    label: "Trener",
    sorter: "surname"
  },
  {
    label: "Kod",
    sorter: "postal_code"
  },
  {
    label: "Miasto",
    sorter: "city"
  },
  {
    label: "Ulica",
    sorter: "street"
  },
  {
    label: "telefon",
    sorter: "tel"
  },
  {
    label: "e-mail",
    sorter: "email"
  },
  {
    label: "K",
    sorter: ""
  }
];

export const rowKeys = [
  "name",
  "postal_code",
  "city",
  "street",
  "tel",
  "email",
  "extra_information"
];

export const type = 'coaches';
