import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Paper from 'material-ui/Paper';
import Grid from 'material-ui/Grid';
import CenteredProgress from 'components/CenteredProgress';
import TrainersPageStyles from 'styles/jss/pages/TrainersPageStyles'
import Table from 'containers/Table/Table';
import RowEditor from 'containers/Table/Row/RowEditor/RowEditor';
import { Consumer } from 'store';
import { labelsMap, rowKeys, type } from './data';


class TrainersPage extends Component {
  render() {
    const { classes } = this.props;
    
    return (
      <Consumer select={['data']}>
      {({ state }) => (
        <Grid item md={10} lg={8} className={classes.wrapper}>
          <RowEditor type={type} newEntity title="Dodaj trenera" />
          <Paper className={classes.root}>
            {state.data.coaches ?
              <Table
                type={type}
                rows={state.data.coaches}
                headerLabels={labelsMap}
                rowKeys={rowKeys}
              />
              : <CenteredProgress size={40} />
            }
          </Paper>
        </Grid>
      )}
      </Consumer>
    );
  }
}

TrainersPage.propTypes = {
  coaches: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    name: PropTypes.string,
    surname: PropTypes.string,
    postal_code: PropTypes.string,
    city: PropTypes.string,
    street: PropTypes.string,
    tel: PropTypes.string,
    email: PropTypes.string,
    confirmed: PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.number
    ]),
    extra_information: PropTypes.string,
  },))
};

export default withStyles(TrainersPageStyles)(TrainersPage);
